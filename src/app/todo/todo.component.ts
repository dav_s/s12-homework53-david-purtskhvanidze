import {Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent {
  @Input() todoText = '';
  @Input() todoCheck = 'active';
  @Output() check = new EventEmitter();
  @Output() delete = new EventEmitter();
  @Output() editTodo = new EventEmitter<string>();

  onTodoInput(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.editTodo.emit(target.value);
  }

  onClickDelete() {
    this.delete.emit();
  }

  onCheck() {
    this.check.emit();
  }

  onFocus(wrap: HTMLInputElement) {
    wrap.classList.add('bordered');
  }

  onBlur(wrap: HTMLInputElement) {
    wrap.classList.remove('bordered');
  }
}
