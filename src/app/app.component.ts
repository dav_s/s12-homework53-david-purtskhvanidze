import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  todoText = '';
  todoCheck = false;
  todoList = [
    {todoText: 'Buy milk', todoCheck: false},
    {todoText: 'Buy coca cola', todoCheck: false},
    {todoText: 'kill Kennedy', todoCheck: false}
  ];


  onAddUser(event: Event) {
    event.preventDefault();
    this.todoList.push({
      todoText: this.todoText,
      todoCheck: this.todoCheck
    });
  }

  onTodoEdit(index: number, $event: string) {
    this.todoList[index].todoText = $event;
  }

  onDeleteTodo(index: number) {
    this.todoList.splice(index, 1);
  }

  onCheckTodo(index: number) {
    this.todoList[index].todoCheck = !this.todoList[index].todoCheck;
  }

  getTodoState(todoCheck: boolean) {
    if (!todoCheck) {
      return 'active';
    } else {
      return 'done';
    }
  }

}
